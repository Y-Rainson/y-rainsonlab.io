---
title: 关于eclipse中的web项目没有部署到tomcat中
---


## 问题描述
在开发java web项目时，右键项目run on server，并选择了tomcat，tomcat启动后访问路径，项目404。查找tomcat中tomcat/webapps目录，发现项目并没有部署到其中。

## 问题原因
eclipse即使配置了tomcat，也不代表eclipse就会把项目部署到webapps中，但可以通过改变配置进行设置。步骤如下：

1.创建server

2.更改web项目的部署位置 

server location改为第二个，并将下面目录名改为webapps。（运行项目是为了启动tomcat，启动之后清空tomcat中的项目，才可以更改server location）