---
title: SpringBoot中集成swagger形成API文档
---

# 环境版本
springboot 1.5.9.RELEASE 
swagger2    2.7.0
# 开始使用
## 1.添加依赖
```java
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger2</artifactId>
	<version>2.7.0</version>
</dependency>
<dependency>
	<groupId>io.springfox</groupId>
	<artifactId>springfox-swagger-ui</artifactId>
	<version>2.7.0</version>
</dependency>
```
## 2.添加Swagger2配置类
```java
package com.chartered;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName Swagger2
 * @Description
 * @Author Y-Rainson
 * @Date 11/29/2018 10:09 AM
 **/
@Configuration
@EnableSwagger2
public class Swagger2 {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.chartered.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("xx项目 RESTful APIs")
                .description("xx项目后台api接口文档")
                .version("1.0")
                .build();
    }

}
```
## 3.在Controller中使用
```java
package com.chartered.controller;

import com.chartered.service.HelloService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName HelloController
 * @Description
 * @Author Y-Rainson
 * @Date 12/3/2018 09:54 AM
 **/
@RequestMapping("/Hi")
@ResponseBody
@Controller
@Api(value = "HelloController", description = "Hi接口")
public class HelloController {
    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    @ApiOperation(value="sayHello", notes="you can use this API to say hello")
    public String sayHello(){
        return helloService.sayHello();
    }

    @RequestMapping(value = "/helloPeople",method = RequestMethod.POST)
    @ApiOperation(value="sayHelloToPeople", notes="you can use this API to say hello to somebody")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="query",name = "name",value = "you name",required = true ,dataType = "String"),
    })
    public String sayHelloToPeople(@RequestParam("name") String name){
        return helloService.sayHelloToPeople(name);
    }
}
```

==注意:==
若接口中有参数的传递，一定要注意添加 ==paramType="query"== 属性，否则会报错
```java
@ApiImplicitParams({
            @ApiImplicitParam(paramType="query",name = "name",value = "you name",required = true ,dataType = "String"),
    })
```

报错如下：
```java
{
  "timestamp": 1543807979427,
  "status": 400,
  "error": "Bad Request",
  "exception": "org.springframework.web.bind.MissingServletRequestParameterException",
  "message": "Required String parameter 'name' is not present",
  "path": "/Hi/helloPeople"
}
```

## 4.访问
浏览器访问
```java
http://localhost:8080/swagger-ui.html
```
效果如下：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20181203113818155.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMwMzA4OTgx,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20181203113839852.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMwMzA4OTgx,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20181203113844829.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMwMzA4OTgx,size_16,color_FFFFFF,t_70)

# 参数是自定义对象
```java
@RequestMapping(value = "/helloUser",method = RequestMethod.POST)
    @ApiOperation(value="helloUser", notes="hello to user")
    public User helloUser(@RequestBody @ApiParam(name="user",value="user entity") User user){
        return helloService.sayHelloToUser(user);
    }
```


并在对应的实体类中加入注解：
```java
package com.chartered.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.katharsis.resource.annotations.JsonApiId;
import io.katharsis.resource.annotations.JsonApiResource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created by 1593977 on 9/4/2018.
 */
@ApiModel(value="User Entity")
public class User {
   
    @ApiModelProperty(value="user id",example="2998324787234897")
    private Long userId;

   
    @ApiModelProperty(value="user email",example="xx@sc.com")
    private String email;

   
    @ApiModelProperty(value="user name",example="jack")
    private String name;

  
}

```

# swagger2+restful接口风格
其他的没有差别，只是restful接口是参数绑定时候
```java
    @RequestMapping(value = "department/{departmentId}", method = RequestMethod.GET)
    @ApiOperation(value="findOne", notes="Use this API to find a department by id.")
    @ApiImplicitParam(paramType ="path",name = "departmentId",value = "department id",required = true ,dataType = "String")
    public ResponseResult findOne(@PathVariable String departmentId) {
        return departmentService.queryOne(departmentId);
    }
```
==这里的paramType ="path" 而不是之前的"query"了！！这点要记得==